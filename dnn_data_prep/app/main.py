"""
This is the implementation of data preparation for sklearn
"""

import os
import sys

import pandas as pd
import numpy as np
import json

import sys
import logging

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="dnn_data_prep",
                   level=logging.INFO)


class DnnDataPrep(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, xpresso_run_name, parameters_filename,
                 parameters_commit_id):
        super().__init__(name="DnnDataPrep",
                         run_name=xpresso_run_name,
                         params_filename=parameters_filename,
                         params_commit_id=parameters_commit_id)
        # if you have specified parameters_filename or parameters_commit_id,
        # the run parameters can be accessed from self.run_parameters
        """ Initialize all the required constants and data here """
        self.parameters_file = parameters_filename
        train_file_path = self.run_parameters["train_file_path"]
        test_file_path = self.run_parameters["test_file_path"]
        store_file_path = self.run_parameters["store_file_path"]
        self.train_data = pd.read_csv(train_file_path, parse_dates=['Date'])
        self.store_data = pd.read_csv(store_file_path)
        self.test_data = pd.read_csv(test_file_path, parse_dates=['Date'])
        self.combined_train_data = None
        self.combined_test_data = None

    @staticmethod
    def expand_date(train):
        """ Expand date into day, week, month , year """
        train['Day'] = train['Date'].dt.day
        train['Week'] = train['Date'].dt.week
        train['Month'] = train['Date'].dt.month
        train['Year'] = train['Date'].dt.year

    @staticmethod
    def is_competition(row):
        """ Use Competition open time to determine if valid competition """
        if row['Year'] >= row['CompetitionOpenSinceYear'] and \
            row['Month'] >= row['CompetitionOpenSinceMonth']:
            return 1
        else:
            return 0

    @staticmethod
    def is_promo2(row):
        """Use promo timestamp to determine if promo is active or not """
        if row['Promo2'] == 1 and \
            row['Year'] >= row['Promo2SinceYear'] and \
            row['Week'] >= row['Promo2SinceWeek']:
            return 1
        else:
            return 0

    def start(self, xpresso_run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            xpresso_run_name: xpresso run name which is used by base class to
            identify the current run. It must be passed. While running as
            pipeline, xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=xpresso_run_name)
            # === Your start code base goes here ===

            ### $xpr_param_pipeline_job_import
            logger.info("Preparing data")
            # We need to correct some badly interpreted StateHoliday values 0 -> "0"
            self.train_data.loc[
                self.train_data['StateHoliday'] == 0, 'StateHoliday'] = "0"
            # Prior to do the join, we quickly modify the store dataframe to set 'Store' as index
            self.store_data.set_index('Store', inplace=True)
            logger.info("Combining train data...")
            combined_train_data = self.train_data.join(self.store_data, on='Store')
            combined_test_data = self.test_data.join(self.store_data, on='Store')
            self.send_metrics("Merged Sales and Customer Data", combined_train_data)
            combined_test_data.loc[np.isnan(combined_test_data['Open']), 'Open'] = 1
            combined_test_data['Open'] = combined_test_data['Open'].astype(int)
            logger.info("Updated open...")
            self.expand_date(train=combined_train_data)
            self.expand_date(train=combined_test_data)
            self.send_metrics("Expanded dates into year, month, week and day",
                              combined_train_data)
            # update is combined train
            combined_train_data['isCompetition'] = combined_train_data.apply(
            lambda row: self.is_competition(row),
                axis=1)
            combined_train_data.loc[
                np.isnan(combined_train_data[
                         'CompetitionDistance']), 'CompetitionDistance'] = 0
            self.expand_date(train=combined_test_data)
            logger.info("Adding competition distance...")
            # update is combined test
            combined_test_data['isCompetition'] = combined_test_data.apply(
                lambda row: self.is_competition(row),
                axis=1)
            combined_test_data.loc[
                np.isnan(combined_test_data[
                             'CompetitionDistance']), 'CompetitionDistance'] = 0
            logger.info("Adding promo...")
            combined_train_data['Promo2'] = combined_train_data.apply(
                lambda row: self.is_promo2(row), axis=1)
            combined_test_data['Promo2'] = combined_test_data.apply(
                lambda row: self.is_promo2(row), axis=1)
            self.send_metrics("Set promotion flag in data", combined_train_data)
            drop_columns = ['Date', 'Week',
                            'CompetitionOpenSinceMonth', 'CompetitionOpenSinceYear',
                            'Promo2SinceWeek', 'Promo2SinceYear', 'PromoInterval']
            combined_train_data.drop(drop_columns + ['Customers'], axis=1,
                                     inplace=True)
            combined_test_data.drop(drop_columns, axis=1, inplace=True)
            self.send_metrics("Dropped 8 irrelevant columns", combined_train_data)
            combined_train_data.drop(
                combined_train_data[combined_train_data['Open'] ==
                                    0].index, inplace=True)
            combined_train_data.drop(['Open'], axis=1, inplace=True)
            combined_test_data.drop(combined_test_data[combined_test_data['Open'] ==
                                                       0].index, inplace=True)
            combined_test_data.drop(['Open'], axis=1, inplace=True)
            self.send_metrics("Dropped rows where Open = 0; Dropped Open column",
                              combined_train_data)
            combined_test_data.insert(loc=2, column='Sales', value=0)
            logger.info("Final update")
            self.send_metrics("Completed Data Preparation", combined_train_data)
            self.combined_train_data = combined_train_data
            self.combined_test_data = combined_test_data
            self.completed()

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    def send_metrics(self, status, combined_data):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"rows": str(combined_data.shape[0]),
                    "columns": str(combined_data.shape[1])
                    }
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp, success=success)
            output_dir = self.run_parameters["dnn_data_prep_out_path"]
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            self.combined_train_data.to_csv(os.path.join(output_dir,
                    "dnn_combined_train.csv"), index=False)
            self.combined_test_data.to_csv(
                os.path.join(output_dir, "dnn_combined_test.csv"),
                index=False)
            logger.info("Data saved")
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    run_name = ""
    params_filename = None
    params_commit_id = None
    if len(sys.argv) >= 2:
        run_name = sys.argv[1]
    if len(sys.argv) >= 4:
        params_filename = sys.argv[2] if sys.argv[2] != "None" else None
        params_commit_id = sys.argv[3] if sys.argv[3] != "None" else None

    pipeline_job = DnnDataPrep(run_name, params_filename, params_commit_id)
    pipeline_job.start(xpresso_run_name=run_name)

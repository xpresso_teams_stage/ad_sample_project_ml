"""
This is the implementation of data preparation for sklearn
"""

import sys
import pickle
import pandas as pd
import numpy as np
import os
import json
import logging

from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.model_selection import RandomizedSearchCV
from xgboost.sklearn import XGBRegressor
import scipy.stats as st

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="xgboost_train",
                   level=logging.INFO)


class XgboostTrain(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, xpresso_run_name, parameters_filename,
                 parameters_commit_id):
        super().__init__(name="xgboost_train",
                         run_name=xpresso_run_name,
                         params_filename=parameters_filename,
                         params_commit_id=parameters_commit_id)
        """ Initialize all the required constants and data here """
        # load parameters
        self.parameters_file = parameters_filename

        self.base_folder = self.run_parameters["xgboost_data_prep_out_path"]
        self.combined_train_data = None
        self.features_train = None
        self.features_test = None
        self.labels_train = None
        self.labels_test = None

    def load(self):
        self.combined_train_data = pd.read_csv(
            os.path.join(self.base_folder,
                         "combined_train.csv"))

        # create X and y
        combined_train_data1 = self.combined_train_data.sample(frac=0.2)
        logger.info(combined_train_data1.head())
        feature_cols = ['CompetitionDistance', 'Promo', 'Promo2',
                        'NewAssortment', 'NewStoreType']
        X = combined_train_data1[feature_cols]
        y = combined_train_data1.Sales
        y1 = combined_train_data1.Customers
        self.features_train, self.features_test, self.labels_train, self.labels_test = train_test_split(
            X, y, test_size=0.3, random_state=42)
        for col in feature_cols:
            logger.info(self.features_train[col].head())
            if np.isnan(self.features_train[col]).any():
                self.features_train.fillna(0, inplace=True)
            if np.isnan(self.features_test[col]).any():
                self.features_test.fillna(0, inplace=True)
            logger.info(np.isfinite(self.features_train).all())
            logger.info(np.isfinite(self.features_test).all())
        logger.info(self.labels_train.head())
        logger.info(np.isfinite(self.labels_train).all())

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            self.load()
            logger.info("Training decision tree regressor")
            logger.info(self.features_train.head())

            one_to_left = st.beta(10, 1)
            from_zero_positive = st.expon(0, 50)

            params = {
                "n_estimators": st.randint(3, 40),
                "max_depth": st.randint(3, 40),
                "learning_rate": st.uniform(0.05, 0.4),
                "colsample_bytree": one_to_left,
                "subsample": one_to_left,
                "gamma": st.uniform(0, 10),
                "reg_alpha": from_zero_positive,
                "min_child_weight": from_zero_positive,
            }
            temp_status = {"status": "Creating XGBRegressor"}
            temp_status.update({key: str(value) for key, value in params.items()})
            self.report_status(status=temp_status)
            mdl = XGBRegressor()
            model = RandomizedSearchCV(mdl, params, n_jobs=1, verbose=10)

            logger.info("Start training")
            model.fit(self.features_train, self.labels_train)
            self.report_status(status={"status": "Training Started"})
            logger.info("Training Completed")
            logger.info("Calculating accuracy")
            logger.info(self.features_test.shape)
            self.report_status(status={"status": "Calculating Accuracy"})
            r2_score_train = r2_score(self.labels_train,
                                      model.predict(self.features_train))
            r2_score_test = r2_score(self.labels_test,
                                     model.predict(self.features_test))
            logger.info("R2Score Train:{} R2Score Test: {}".format(r2_score_train,
                                                             r2_score_test))
            self.report_status(status={"status": "Accuracy Calculated",
                                       "accuracy": r2_score_test,
                                       "accuracy_train": r2_score_train,
                                       })
            if not os.path.exists(self.OUTPUT_DIR):
                os.makedirs(self.OUTPUT_DIR)
            with open(os.path.join(self.OUTPUT_DIR, "xgboost.pkl"), "wb") as model_fs:
                pickle.dump(model, model_fs)
            logger.info("Saved model... load model")

            super().completed(push_exp=True)
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    run_name = ""
    params_filename = None
    params_commit_id = None
    if len(sys.argv) >= 2:
        run_name = sys.argv[1]
    if len(sys.argv) >= 4:
        params_filename = sys.argv[2] if sys.argv[2] != "None" else None
        params_commit_id = sys.argv[3] if sys.argv[3] != "None" else None

    trainer = XgboostTrain(run_name, params_filename, params_commit_id)
    trainer.start(run_name=run_name)

